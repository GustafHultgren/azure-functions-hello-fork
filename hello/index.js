module.exports = function (context, req) {
    // this is a feature
    context.res = {
        status: 200,
        body: 'hello world!',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    context.log('hello world');
    context.done();
};