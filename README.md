# README #

Azure Functions Hello World!

### What is this repository for? ###
Simple hello world function app

### How do I get set up? ###

Install azure functions core tools via npm:
`npm install -g azure-functions-core-tools`

Run the server at default port 7071:
`func host start`

Go to your browser and visit http://localhost:7071/api/hello